\chapter{Presenting Business to the Assembly}

		The most common way to present business to the members at a meeting is to make a main motion. An idea is not discussed first and then a motion made; instead, a motion is made and then the idea is discussed. This chapter explains the basic steps in presenting a motion: how to make a main motion, how to discuss it, and how to take a vote on the motion. It also explains when a motion is out of order. For example, motions are out of order when they conflict with the rules of the organization or the laws of the land, or when they propose action outside the scope of the organization’s objectives.
		
		\section{Basic Steps in Presenting a Motion}
		
		Before you present a motion, make sure that it contains all the pertinent information, including who, what, where, and when. Word the motion in the positive, not in the negative. Here is an example of a main motion:
		
		\begin{quote}
			\textbf{Member:} Madam Chairman, I move that we have a picnic on Saturday, June 15, at 3 p.m. in the park.
		\end{quote}
			
	\begin{enumerate}
			\item A member stands and addresses the chair, saying:
				\begin{quote}
					\textbf{Member:} Mr. President [or Madam President]
					
					 or
					
					\textbf{Member}: Mr. Chairman [or Madam Chairman]
				\end{quote}
						
			\item The chair assigns the member the floor by stating the member’s name or nodding at the member. The member states the motion:
			\begin{quote}
				\textbf{Member:} I move that/to...
			\end{quote}
		
			\item Another member seconds the motion by calling out:
				\begin{quote}
					\textbf{Member:} I second the motion.
				\end{quote}		
		
			\item Another member seconds the motion by calling out:
				\begin{quote}
					\textbf{Chairman:} It is moved and seconded that \textellipsis Is there any discussion?
				\end{quote}
			
			\item The members now have the right to debate or discuss the motion.
			
			\item When the discussion is finished, the chair puts the motion to a vote by saying:
				\begin{quote}
					\textbf{Chairman}: All those in favor say “Aye.” Those opposed say “No.”
				\end{quote}
			
			\item The chair announces the vote and who will carry out the action if it is adopted.
	\end{enumerate}

	\section{Making a Main Motion}
	
	To make a main motion, a member must obtain the floor. To do so, stand and address the chairman. This is the correct parliamentary terminology. Many people want to say \emph{chairwoman} or \emph{chairperson}, but these terms are incorrect. The English language does not have feminine or masculine words, as do the Latin-based languages. The word \emph{chair} in English is the neuter gender, neither masculine nor feminine. It refers either to the person or the place (chair) occupied by the person. The word \emph{man} at the end does not mean a masculine person but stands for the neuter gender all \emph{mankind}, including both men and women. So in English, to acknowledge the gender of the person presiding in the chair, use the honorifics \emph{Mr.} or \emph{Madam}, as follows: \emph{Mr. Chairman} or \emph{Madam Chairman}.
	
	After the chair is addressed, the presiding officer recognizes the member by saying his or her name or nodding at the member. This means that the member is assigned the floor and can speak.
	
	State motions in the positive, not the negative. Write the motion on a piece of paper and give it to the chairman after you state it. This helps the chairman (or chair) repeat the motion to the assembly exactly the way it was moved. The way in which the presiding officer states the motion to the assembly is the official wording of the motion, and it’s recorded in the minutes. Many times, presiding officers do not repeat the motion exactly as the member has stated it. If you put the motion in writing and give it to the presiding officer, the officer can repeat it exactly as you presented it, and the secretary can record it correctly in the minutes.
	
	After you make the motion, sit down. Another member must second the motion. A \emph{second} simply means that another member thinks the motion should be discussed. It does not mean that the member is necessarily in favor of it. The person who seconds the motion does not need to rise and address the chairman but can call out the second from where he or she sits. If no one seconds it, the chairman can ask:
		\begin{quote}
			\textbf{Chairman:} Is there a second?
		\end{quote}
	If the motion does not get a second, members can’t discuss it and the chairman goes on to the next business in order. The chairman can say:
		\begin{quote}
			\textbf{Chairman:} Without a second, the motion will not be considered.
		\end{quote}
	If the motion is seconded, the chairman restates the motion to the members. This is called placing the motion before the assembly. The chairman must do this so that members can discuss the motion. The chairman says:
		\begin{quote}
			\textbf{Chairman:} It is moved and seconded that \textellipsis Is there any discussion?
		\end{quote}
	
	\section{Discussing a Motion}
	
	Members always have the right to debate or discuss a main motion. The person who makes the motion has the first right to speak to the motion. To do that, the member rises, addresses the chairman, obtains the floor, and then speaks to the motion. After the member is done, he or she sits down so that someone else can speak to the motion—either for or against it. In discussing the motion, everyone gets to have a turn to talk, but everyone must wait his or her turn. A member can speak to a motion only when no one else is assigned the floor. If two people stand to speak at the same time, the chairman designates who should speak. The member not recognized sits down. When the other member finishes speaking, the member who did not speak can then stand to speak.
	
	In debate, members address all remarks through the chair. Cross talk between members is not allowed, and mentioning other members’ names is avoided as much as possible. All remarks are made in a courteous tone.
	
	In most meetings, each member may speak two times on a debatable motion, but the member does not get the second turn as long as another member wants to speak for the first time. A member is not permitted to speak against his or her own motion. However, if the member changes his or her mind after hearing the motion discussed, the member may vote against it.
	
	When speaking to a motion that you haven’t made, a common courtesy before you begin your remarks is to say:
		\begin{quote}
			\textbf{Member:} I speak for the motion.
			
			or
			
			\textbf{Member:} I speak against the motion.
		\end{quote}
	This way, the assembly knows which side of the issue you are supporting. It also helps the chairman keep a balance in the debate. If there are more people speaking against the motion, the chairman may ask if anyone wants to speak for the motion.
	
	In debate, everyone has the right to speak, and the chairman must be just and impartial in assigning the floor, allowing all sides of the issue to be heard. Discussion continues until the chairman realizes that the membership is ready to vote.
	
	\section{Taking the Vote}
	
	When no one rises to speak to the motion, the chairman calls for the vote. Most voting takes place via a voice vote. A majority vote adopts main motions, which means that more than half of the members voting favor the motion.
		\begin{quote}
			\textbf{Chairman:} Is there any further discussion? [Pause and look around the room to see if anyone wants to speak.] Hearing none, the question is on the adoption of the motion to \textellipsis
		\end{quote}
	The chairman asks only for the yes and no votes and does not ask for those who want to abstain. The chairman always takes the no vote, even if the yes vote sounds unanimous.
	
	The members must feel that any vote taken is a fair vote. If any member doubts the results of a voice vote, the member can call out:
		\begin{quote}
			\textbf{Member:} I call for a division.
			
			or
			
			\textbf{Member:} I doubt the result of the vote.
		\end{quote}
	In this one instance, a member does not have to rise to obtain the floor but can call out “Division” from wherever he or she is sitting. It does not need a second.
	
	The chairman immediately retakes the vote as a rising vote by asking the members to stand. The chair makes a visual judgment and does not count the vote.
	
	If the chairman is in doubt as to which side wins, he or she can retake the vote and have it counted. If a member wants the vote counted, the member makes a motion to have a counted vote. The motion requires a second, is not debatable, and must pass by a majority vote.
	
	In addition to a voice vote, the organization can take a vote by general consent, a show of hands, a rising vote, or ballot. The chair can choose to take the vote by voice, show of hands, or rising. To take a ballot vote, a member must make a motion to do so. A ballot vote ensures the secrecy of each member’s vote. If you do not want others to know how you voted, or if you want an accurate count of the vote, a ballot vote is the way to accomplish your goal.
	
	To ask for a ballot vote, a member must rise, address the chair, and move to take the vote by ballot. This motion needs a second, is not debatable, and must pass by a majority vote.
	
	\section{Completing the Action on the Motion}
	
	The action on the motion is completed when the chairman announces the result of the vote as well as how the action will be carried out. Members can expect that the approved action is carried out as authorized unless they decide to \emph{reconsider the vote, rescind the action,} or \emph{amend the adopted motion}.
	
	\section{Important Points to Remember Before Making a Motion}
	
	Not every main motion is in order, and both the members and the presiding officer need to know when a presented motion violates the following rules. If a main motion violates the following rules, it is the presiding officer’s duty to rule the motion out of order. If the chair does not do this, a member should call this to the assembly’s attention by raising a point of order.
	
	\begin{enumerate}
		\item No motion is in order that conflicts with state, national, regional, or local law; with the rules of a parent organization; or with the organization’s constitution or bylaws or other rules of the organization. Even if a unanimous vote adopts the motion, it is null and void if it conflicts with the previously mentioned rules.
		\item A motion that proposes action outside the scope of the organization’s object (which should be written in the bylaws) is not in order unless the members vote to allow it to be considered. Doing so takes a two-thirds vote.
		\item A main motion is not in order if it conflicts with a motion that was previously adopted by the assembly and that is still in force. However, the assembly can decide to rescind the action or amend something previously adopted.
		\item A main motion is not in order when it presents substantially the same question as a motion that was rejected during the same session. However, members can bring up the motion at another meeting, and this is known as \emph{renewing the motion}.
		\item A main motion is not in order if it conflicts with or presents substantially the same question as one that has been temporarily disposed of and is still within control of the assembly.
	\end{enumerate}

	\section{Resolutions}
	
	A \emph{resolution} is a formal way of presenting a motion. It is a main motion, needs a second, and is handled like any other main motion except that it is always presented in writing. The name of the organization is mentioned in the resolution, and the word “resolved” is always italicized. A resolution can be as simple as:
		\begin{quote}
			\textit{Resolved}, That the Glee Club sponsor a “Day of Singing” on April 25 to honor Glee Clubs in our state.
		\end{quote}
	Sometimes a resolution includes a preamble. A \emph{preamble} enables members to give background information and to state the reasons why the motion should be adopted. However, a preamble to a resolution is usually not necessary. In fact, a preamble should be used only when the maker of the resolution wants to give little-known information or wants to present important points regarding the adoption of the motion if there is some doubt about whether it will pass. A preamble contains \emph{whereas} clauses that communicate the important background information to the assembly; the actual resolution then follows. A resolution with a preamble should contain only as many whereas clauses as necessary.
	