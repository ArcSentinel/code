\chapter{Meetings}

	A meeting, as defined by \emph{Robert’s Rules of Order}~\cite{robert2011}, is a single official gathering of the members of an organization in one room, with a quorum present to transact business. The members do not leave the meeting, except for a short recess, until the business has been completed or the chair declares the meeting adjourned. This chapter covers the many types of formal meetings (including conventions and mass meetings) and informal meetings (small board and committee meetings), and it discusses the pros and cons of electronic meetings. It also explains how to manage and evaluate meetings and how to form strategies—and counter-strategies—for meetings.

	\section{The Basics}
	
	All meetings, regardless of size or purpose, have some things in common:
	\begin{itemize}
		\item A quorum must be present.
		\item Someone is in charge of conducting the meeting.
		\item Someone is responsible for taking the minutes.
		\item Business is conducted according to specific rules that state who can attend, who can participate in the discussion of business, and who can vote.
		\item All members are notified of the meeting’s date and time and the purpose for which the meeting is called. The parliamentary term for this procedure is the call to the meeting.
	\end{itemize}

	Most organizations have both formal and informal meetings. A \emph{formal meeting} is one in which the entire membership meets to hear reports of officers, boards, and committees, and to propose business, discuss it, and vote on it. An \emph{informal meeting} is one in which a small group of the organization meets, in either committee meetings or small boards, to help the organization carry out its goals.
	
	Meeting procedures vary according to the meeting type. The primary difference between formal and informal meetings is the chair’s role in the meeting. In formal meetings, the members usually follow strict parliamentary procedures, which means that the chair stands while presiding and while stating a motion and taking the vote. He or she does not participate in the debate unless he or she leaves the chair. Members must rise and be recognized in order to obtain the floor, make motions, and debate. Debate is restricted to ten minutes each time a member speaks unless there is a rule to the contrary, and each member may speak twice to a motion.
	
	In informal meetings, the person presiding is usually seated and takes an active role in making motions, discussing them, and voting on all issues. There are usually no limits on debate, and members can discuss an issue without a formal motion.
	
	\section{Meeting Strategies}
	
	This section offers plans of action for either adopting a motion or defeating a motion while preserving the highest ideal of democratic proceedings in meetings. Although the word \emph{strategy} has a favorable definition—a careful plan or method to achieve a goal—it can also connote a plan of action to defeat an enemy by trickery.
	
	The intent of this discussion is not subterfuge but information. This section describes the procedure or procedures that counteract any motion presented in a meeting. There are always two sides (sometimes many sides) to each issue, and if one group prevails by subverting the democratic process and preventing the other side(s) from being heard, the action is doomed to fail. Often such action leads to the breaking up of the organization. However, each side of an issue has the right to use parliamentary procedures in a fair, just, and honest way to obtain its goal.
	
	All members should know fully the rules of procedure, their rights as members, and how to protect those rights. Remember that if an opposition sleeps on its rights, it may be too late to correct the action. Therefore, members must pay attention to what is happening in the meeting and point out to the chair and the assembly any mistakes when they happen—not after the meeting has adjourned and everyone has gone home.
	
	When organizations have to consider a controversial issue, both sides commonly come to the meeting with a plan to achieve their goals. If the organization is divided on the issue and the presentation of the debate was equal, usually the side having the best understanding of parliamentary procedure wins. Therefore, all members should understand the types of strategies and how to counter each one.
	
	There are several types of meeting strategies: those necessary to adopt, delay, or defeat an action; to bring a compromise; and to change an action.
	
	One of the most important strategies is to come prepared for the meeting. The person who will make the motion should already be assigned and rehearsed, have the motion carefully worded, and be prepared for the debate. During rehearsal for debate, ask someone to play devil’s advocate so that members can counter the opposition’s objections. Plan what you will do when motions are made to defeat the proposed action; if possible, work with a parliamentarian. The same advice goes for those opposing the action.
	
	If you are the person who has to preside during a controversy, think about all the situations and motions that can arise, and prepare for them. Be ready to keep order and handle points of order and appeals from the decision of the chair. Seek advice from a parliamentarian before the meeting and, if possible, hire a parliamentarian to help you during the meeting.
	
	\subsection{Strategy to Adopt an Action}
	
	Often a proposed action comes from a committee or the board of an organization. If this is the case, the great advantage is that the proposal comes from an official organ of the organization. In the eyes of the membership, such a proposal usually carries more authority than if an individual membermakes the motion. There is also more than one person supporting the idea. The other advantage is that in presenting the idea to the members, the committee or board gives a report explaining the whys and wherefores of the motion. The committee can meet many objections in the report by providing enough information. The other advantage that a board or committee has is the ability to give to the assembly written information attached to the agenda or sent out with the call to the meeting.
	
	An individual presenting a motion isn’t able to give a report or attach printed materials to the agenda without the permission of the assembly. Nor is an individual likely to have a small group of supporters already in favor of the action. However, one way to get more information to the group about the motion is to present it in the form of a resolution. In this form, the preamble can state the reason for the proposed action.
	
	A defensive strategy in adopting a motion is to watch for those who may try to shut off debate prematurely, or those who try to kill a motion by laying it on the table instead of making the motion to postpone indefinitely. During the meeting, if you feel that you are losing ground or you need to consult with others, make a motion to recess. If the members seem to need more time to think about the motion, or the committee needs to gather more facts, use a delaying strategy.
	
	\subsection{Strategy to Delay an Action}
	
	Delaying an action is sometimes the wisest move to make. There are two ways to delay an action: first by referring the motion to a committee and second by postponing it to a later time. (Sometimes the motion to reconsider the vote delays the action if this motion is not taken up at the meeting but is called up at the next meeting.)
	
	A delaying strategy is helpful if a member makes a motion that no one is prepared to discuss or has even thought about. In this instance, the motion to postpone to the next meeting enables the members to gather information, formulate their reasons pro or con, and get the absent members to attend the next meeting. In cases where the members are uncertain because they don’t have enough information, or when they want to know how the details are going to be worked out, it is best not to push for a decision at that meeting but to make a motion to refer to a committee.
	
	\subsection{Strategy to Defeat a Motion}

	Members don’t always agree, and those who oppose an action have the right to try to defeat it. The most obvious ways to defeat an action are to:
	\begin{itemize}
		\item Debate against it.
		\item Postpone it indefinitely.
		\item Postpone it to another meeting, hoping that members can gather more support to vote against it.
	\end{itemize}

	However, if something detrimental to the organization or a member is proposed, the wisest action is to object to consideration of the question immediately after the chairman states it. This motion should seldom be used, and only when something would truly harm the organization even to discuss it.
	
	\subsection{Compromise as a Strategy}
	
	An alternative to defeating a motion is to compromise. Very rarely is an issue a \emph{yes or no} question. Most issues are negotiable and can be resolved through discussing, carefully listening to others, and then using the amending process. For example, say that the membership is divided about the time of the meetings. Group A wants the meetings changed to an earlier time, but Group B wants to keep the current time. After much discussion and proposing of amendments, the members vote on a time that neither one really wants but that is between the current meeting time and Group A’s suggestion. It is a compromise that everyone can accept.
	
	\subsection{Other Strategies}
	
	Besides the strategies already discussed, there are others that an organization can use to effectively change an action, correct a mistake, cool down tempers, keep a motion alive, or deal with various situations arising in a meeting:
	\begin{itemize}
		\item \textbf{A point of order:} A point of order can correct a multitude of errors. If the chair does not correct a serious mistake in a meeting, a member can raise a point of order. Members should use this motion only when a serious mistake has been made in the meeting; for example, when members’ rights are being taken away. The chair always rules on the point of order before proceeding with any further discussion or motions. If a member does not agree with the chair’s ruling, he or she should appeal the decision of the chair.
		\item \textbf{Parliamentary inquiry:} If a member realizes that the assembly doesn’t understand what is going on, he or she can rise to parliamentary inquiry, which he or she can do at any time. This is not considered debate, so the member can make it while a nondebatable question is pending. This action may help, for example, when someone has moved the previous question to try to cut off a member’s right to debate. A member can rise and ask the chair what will happen if the members vote for the previous question.
		\item \textbf{Point of information:} A member can raise a point of information anytime during debate to ask for factual information. It is not considered debate, so a member can lead the discussion without debating by asking all the right questions.
		\item \textbf{Motion to recess:} A motion to recess can help cool things off in debate or can provide time to plan strategies with those who are of like mind. Or, it can buy time to call in additional support from members not present. If someone moves the previous question early in debate, a motion to recess can rally votes to stop this motion. If things aren’t going your way, move to take a recess.
		\item \textbf{Move the previous question:} When nothing new is being said but debate is lingering, make the motion to stop debate by saying “I move the previous question.” If adopted, this closes debate and brings the motion to a vote.
		\item \textbf{Lay the pending motion on the table:} If there is a motion being debated on the floor and a member needs to leave the meeting early but wants to make a motion before leaving, he or she can move to lay the pending motion on the table. If adopted, the pending motion will be temporarily laid aside and the member can now make the new motion. Once this issue is decided, someone can move to take the original motion from the table. If the motion is adopted, the assembly proceeds with the meeting where it left off.
		\item \textbf{Postpone to later in the meeting:} f a motion is being discussed and a member who has important information about the subject has not arrived, a member can move to postpone the motion to later in the meeting.
		
		Or, if a member is not at the meeting but another member knows that the member would come to the meeting with a simple telephone call, he or she can make a motion to recess. During the recess, this person can call the member and find out when the member will arrive at the meeting. He or she can then make the motion to postpone until later in the meeting to give the member time to arrive. If the member can’t make the meeting, another member can make a motion to postpone the vote to the next meeting.
		\item \textbf{Reconsider and enter on the minutes:} When a temporary majority resulting from an unrepresented attendance at the meeting pushes through a motion that many absent members would have opposed, the best strategy is to vote on the prevailing side and then move to reconsider and enter on the minutes (see Chapter 17.) This motion needs a second. It stops all action on the motion, which members can bring up only at the next meeting. The call letter for the next meeting must include a notice about this motion.
		\item \textbf{Fix the time to which to adjourn:} If members want to go home but important business still needs to be discussed which may die if the meeting adjourned because of a time element, set the time for an adjourned meeting. To do this, make the motion to fix the time to which to adjourn. Set the hour, date, and place for the meeting. You can then make the motion to adjourn. At an adjourned meeting, business is taken up where the members stopped at adjournment.
		\item \textbf{Consider the intent of a motion:} When a motion is not worded in proper parliamentary terms, consider the intent of the motion. For example, a member may move to table something to the next meeting. This is really the motion to postpone to the next meeting. If the chair places it before the assembly as the motion to lay on the table, a member should raise a point of order because it is taking away the members’ right to debate, and it only requires a majority vote. Another example is if a member makes a motion that would, in effect, rescind something previously adopted where no previous notice was given. Alert members will point out that the vote needed to adopt this motion is a two-thirds vote or a majority of the entire membership. By understanding intent, the membership knows the proper rules governing any situation.
	\end{itemize}

	\subsection{Voting Strategies}
	
	If a voice vote is taken and a member feels that the vote is not decisive, he or she should call for a division. The chair must retake the vote by asking the members to rise. If a question is controversial and a member thinks having a secret vote would deliver the most honest and representative vote, he or she should move to take the vote by ballot. This motion needs a second, is not debatable, and requires a majority vote.

	 \textbf{Requests for clarification:} When you don’t understand what is going on in a meeting or you’ve gotten lost in the procedures, the best strategy is to ask the chair by rising to a parliamentary inquiry. If more members use this tool, organizations can prevent much misunderstanding in meetings.
	 
	 Another way to use a parliamentary inquiry is to ask the chair if it is in order to do something. The chair’s duty is to assist the members in presenting business.
	 
	 Still another way of asking for help in a meeting is by requesting a point of information. If you want more facts about the subject being discussed or don’t understand what someone has just said, ask. Be assured that if you didn’t understand something or are lost in the procedures of a meeting, others are probably having the same problem.
	 
	 