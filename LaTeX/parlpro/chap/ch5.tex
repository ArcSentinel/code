\chapter{Voting}

	In democratic societies, citizens have the right to assemble, the right to speak, and the right to vote. The \emph{right to assemble} allows people of common interests to join together to accomplish a goal or common purpose. The \emph{right to speak} allows members of that assembly to voice their opinions and concerns and to persuade others that their opinions and concerns are valid and to take action. The \emph{right to vote} is the assembly’s way of allowing all members to decide an issue, in a democratic manner, after they have assembled and heard their fellow members’ opinions and concerns.
	
	The right to vote is essential in preserving democracy in organizations and elected bodies. There are three principles that require consideration when a vote is taken:
		\begin{itemize}
		\item Is the vote taken in a fair and impartial manner?
		\item Does everyone who wants to vote get to vote?
		\item Does the announced result represent the way the members voted?
		\end{itemize}
	
	\section{Procedure for Taking a Vote}
	
	In taking a vote, the presiding officer or chair must follow an established general procedure:
	
	\begin{enumerate}
		\item The chair always asks for the affirmative vote first.
		\item The chair always asks for the negative vote, even if the affirmative vote seems unanimous.
		\item The chair does not ask for abstentions.
		\item The chair always announces the result of the vote and states what has just happened. If the affirmative won,
		the chair also states who is responsible for carrying out the action. If the chair is in doubt about who should carry out the action, the members need to make a motion to determine who should carry it out.
		\item The chair must stay neutral in asking for the vote so as not to sway the membership.
		\item The chair does \emph{not} phrase the vote this way:
			\begin{quote}
				\textbf{Chairman:} All those in favor say “Aye.” Those opposed “same sign.”
			\end{quote}
		If the vote were taken this way, it would mean that both those in favor and those opposed would say “Aye.” So then who wins?
		\item It is understood that during all methods of voting a quorum must be present.
		\item If the chair is in doubt about the result of the vote, the chair can request a rising vote or a rising and counted vote to retake the vote.
		\item If a member doubts the result of the vote, the member should call out “division.”
	\end{enumerate}

	\section{The Majority Rules}
	
	A fundamental principle in democratic societies is that the majority rules, but the rights of the minority and individual members are also protected. Most business is adopted by a majority vote of members who are voting at a meeting where a quorum is present. However, to protect the rights of the minority and absent members, some motions require a twothirds vote. The principle used to determine when to take a two-thirds vote is based on the rights of the members or the assembly. When a proposed action takes away members’ rights, a two-thirds vote is necessary. Motions to limit or extend debate, to close debate, to make a motion a special order, to rescind an action when no previous notice is given, and to suspend the rules are some of the motions that require a two-thirds vote. Some actions are so important (for example, amending the bylaws and other governing documents, or removing a member from office or membership) that they require both previous notice and a two-thirds vote.
	
	If an organization wants the vote on certain issues to be greater than a majority or two-thirds vote, or it wants to require that previous notice of a vote be given, the bylaws should clearly state and define these qualifications. However, requiring a vote higher than a majority or two-thirds vote can allow a minority to rule instead of the majority, and a unanimous vote may end up allowing one person to rule. A vote requiring more than a majority should not be stated in terms of a “super majority” (because that term is not specific) but should specify, for example, 80\% of the members, three-fourths of the members, or a majority of the entire membership.
	
	\subsection{Majority Vote Defined}
	
	A \emph{majority vote} simply means that more than half of those voting approve a motion. More specifically, it means that more than half of the votes cast by persons legally entitled to vote at a properly called meeting with a quorum present approve a motion. Blank ballots or abstentions do not count. By this definition, those voting—not necessarily those present—determine the majority. Here is an example:
		\begin{quote}
			If 20 people are present at the meeting and 15 members vote, the majority is 8, because the majority is determined by the number voting, not by the number present.
		\end{quote}
	
	\subsection{Modifications in Majority Vote}
	
	Organizations can qualify a majority vote by adding these phrases to the word “voting” in their bylaws:
	\begin{itemize}
		\item “a majority of those present”
		\item “a majority of the entire membership”
	\end{itemize}

	These phrases change how the organization figures the	majority. The more qualified the bylaws make a majority vote, the more difficult it is to adopt motions. Except for important issues and amending the bylaws, a simple, unqualified majority vote should adopt all actions.
	
	\subsection{A Majority of those Present}
	
	If an organization’s bylaws state that a majority of those present must adopt a motion, the majority is figured by the number of members present, not by the number of those voting. Here is an example:
		\begin{quote}
			A meeting has 20 members present.
			
			A majority of those present is 11 votes. This number does not change, no matter how many members present vote. If 15 people vote, and the following occurs:
			
			\begin{center}
				\begin{tabular}{l c}
					10 members vote in the affirmative & \\
					5 members vote in the negative & \\
					5 members do not vote (abstain) & \\
				\end{tabular}
			\end{center}
			
			the motion fails because 11 people must vote in favor of the motion to sustain a majority of those present and adopt the motion.
		\end{quote}
	In this example, those not voting are said to support the negative rather than remain neutral. For that reason, this qualification is not recommended.
	
	\subsection{A Majority of the Entire Membership}
	
	If the bylaws state that a majority of the entire membership must adopt a motion, the number of the entire membership determines the majority, not the number of members who are present or the number of members who vote. Here are some examples:
		\begin{quote}
			The membership of the organization is 40. 
			
			The majority is always 21 votes in the affirmative. At a meeting, 21 members attend. All would have to vote in favor of a motion in order for it to be adopted. If only 20 attend the meeting, no motions can be adopted because it takes 21 votes to adopt.
			
			If 30 members attend the meeting, and the votes are
			
			\begin{center}
				\begin{tabular}{l c}
					20 votes in the affirmative & \\
					7 in the negative & \\
					3 do not vote (abstain) & \\
				\end{tabular}
			\end{center}
			
			the motion is lost because it takes 21 votes to adopt.
		\end{quote}
	Requiring a majority of the entire membership is a helpful and useful qualification in one case: when the board is very small. Suppose an organization has an executive board of five members and the quorum is three members. If the bylaws state that a majority vote must adopt all action, and if only three members come to a meeting, two members are the majority and can make a decision. In this case, it is appropriate for the bylaws to require a majority vote of the entire membership of the board. Doing so ensures that if only three members attend a meeting, all three have to agree before any action is adopted.
	
	\emph{Note:} When the bylaws state “a majority of the entire membership,” this means a majority of the members who are qualified to vote. Some organizations have a provision in the bylaws that states, “Members who have not paid their dues or are on probation cannot vote.” Members who are not eligible to vote are not counted when figuring the number of votes required to get a “majority of the entire membership.”
	
	\subsection{A Majority of the Fixed Membership}
	
	f the governing documents state “a majority of the fixed membership,” a majority is based on the total number of members, whether they are eligible to vote or not. For example, in condominium or homeowner associations, the majority is often based upon the number of lots or units in the association. This is considered a majority of the fixed membership. In this case, if the owner of a lot or unit does not pay the assessment, it does not affect the number of votes required to achieve the majority of the fixed membership. The number of votes required for a majority doesn’t change unless more units are built or more lots are made available for sale.
	
	The requirement to have a majority of the fixed membership can affect boards of directors if there are unfilled vacancies on the board. A majority of the fixed membership is based on the total number of the board positions, not the total number of persons serving in them. In contrast, if the majority is of the entire membership of the board, vacancies don’t count. Only those positions in which there are directors serving are considered in the total number of board members. For example, if a board has 12 positions and three positions are vacant, and the basis is a majority of the entire membership, the count is based on the nine positions that are filled. The majority of the entire membership is five. However, a majority vote of the fixed membership is seven because it is based upon all 12 directors’ positions, whether or not they are filled. When establishing the vote in the bylaws, take special care with how qualifications for the majority are worded. The more stringent you make the majority, the more difficult it is to obtain the majority and get things done.
	
	\section{A Two-Thirds Vote}
	
	In keeping with accepted parliamentary procedure, there are times when a two-thirds vote is required. This means that at a meeting where a quorum is present, it takes two-thirds of those voting in the affirmative to adopt a motion. Those who abstain are not counted.
	
	To balance the rights of the individual member with the rights of the assembly, the following procedures require a two-thirds vote:
		\begin{itemize}
			\item Limiting or closing debate
			\item Suspending or modifying a rule or order previously adopted
			\item Taking away membership or office
			\item Motions that close nominations or the polls
			\item Preventing the introduction of a motion
		\end{itemize}
	The two-thirds vote is taken by a rising vote. If the chair is uncertain whether there is a two-thirds vote in the affirmative, he or she should count those voting.
	
	\section{A Three-Fourths Vote}
	
	Some organizations require a three-fourths vote instead of a two-thirds vote in adopting certain types of business, electing officers, or electing applicants into membership. These organizations want to know that most of the members agree with what is proposed. They believe that the more members that are in favor of any proposal, the better the cooperation they will get in carrying out what is adopted. An easy way to figure this vote is to remember that every no vote needs three yes votes.
	
	\section{The Tie Vote}
	
	A tie vote occurs when 50\% vote in favor and 50\% vote against. No one receives a majority vote. If there is no way to break the tie vote, the motion is lost.
	
	If the presiding officer has not voted and is a member of the assembly, he or she can vote to make or break the tie. He or she can also vote to make a two-thirds vote or to reject a two-thirds vote. If 50 members vote for the motion and 49 members vote against the motion, the presiding officer can state that he or she votes no, meaning that the vote is a tie and the motion is therefore lost. The presiding officer cannot vote twice, however—once as a member and once as the presiding officer. For example, if the presiding officer votes in a ballot vote with the other members and the result is a tie, the officer can’t break the tie as the presiding officer. In this case, the motion is lost because the vote is a tie vote.
	
	\section{Doubting the Result of the Vote}
	
	It is the presiding officer’s duty to announce the result of the vote, and the way he or she announces it determines the action taken. If the members do not immediately doubt the result of the vote, the chair’s declaration stands as the decision of the assembly. The members have the right to doubt the result of the vote until the chair states the question on another motion.
	
	\subsection{Calling for a Division}
	
	If a member thinks that the vote is too close to call or that the noes have it, and the chair announces the ayes have it, the member can call out “division”.
	
	This should not be used as a dilatory tactic to delay the proceedings when it is apparent which side has won.
	
	A division is an incidental motion: It deals with a procedural
	question relating to a pending motion or business. It does not need a second and is not debatable. One member can ask to retake the vote, and the vote is never retaken in the same way. Thus, a doubted voice vote must be retaken visually, by a rising vote. This way, all the members can see how people are voting.
	
	The chair then announces the vote.
	
	If the vote still looks too close to call, the chair can retake it by having it counted. If a member wants the vote to be counted, he or she makes a motion to take a counted vote. The motion to take a counted vote needs a second, is not debatable, and takes a majority to adopt.
	
	The chair retakes the vote by first asking those in favor of the motion to stand and count off. The chair then asks those
	opposed to a motion to stand and count off. (The member sits down as he counts off.) After all have voted, the chair announces the vote. The vote should be recorded in the minutes by saying how many have voted in the affirmative and how many have voted in the negative.
	
	\subsection{Doubting the Result of a Ballot Vote or Roll Call Vote}
	
	If the members doubt the result of a ballot vote or roll call vote, a member must make a motion to recount the teller’s tabulation. This motion takes a majority to adopt unless the organization has a rule that states differently. After a ballot vote, if there is no possibility that the assembly may order a recount, a motion should be made to destroy the ballots; or they can be filed for a specified time with the secretary and then destroyed.
	
	Record the result of every ballot and roll call vote in the minutes.
	
	\includepdf[pages={93-95}]{../roberts}