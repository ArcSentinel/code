\contentsline {chapter}{Contents}{i}{section*.1}%
\contentsline {chapter}{List of Figures}{ii}{section*.2}%
\contentsline {chapter}{List of Tables}{ii}{section*.3}%
\contentsline {chapter}{Acknowledgments}{iii}{chapter*.4}%
\contentsline {chapter}{Abstract}{iv}{chapter*.5}%
\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Background of the Study}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Objectives}{2}{section.1.2}%
\contentsline {section}{\numberline {1.3}Statement of the Problem}{2}{section.1.3}%
\contentsline {section}{\numberline {1.4}Hypothesis}{2}{section.1.4}%
\contentsline {section}{\numberline {1.5}Significance of the Study}{3}{section.1.5}%
\contentsline {section}{\numberline {1.6}Scope and Delimitation}{3}{section.1.6}%
\contentsline {chapter}{\chapternumberline {2}Review of Related Literature and Studies}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}Discussion of Related Literature}{4}{section.2.1}%
\contentsline {section}{\numberline {2.2}Gap Bridged by the Study}{4}{section.2.2}%
\contentsline {section}{\numberline {2.3}Conceptual Framework}{4}{section.2.3}%
\contentsline {section}{\numberline {2.4}Theoretical Framework}{4}{section.2.4}%
\contentsline {section}{\numberline {2.5}Definition of Terms}{4}{section.2.5}%
\contentsline {chapter}{\chapternumberline {3}Methodology}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}Time and Locale of the Study}{5}{section.3.1}%
\contentsline {section}{\numberline {3.2}Research Design and Methods}{5}{section.3.2}%
\contentsline {section}{\numberline {3.3}Materials and Sampling}{5}{section.3.3}%
\contentsline {section}{\numberline {3.4}Equipment and Research Instrument Validation}{5}{section.3.4}%
\contentsline {section}{\numberline {3.5}Data Gathering and Experimental Procedures}{5}{section.3.5}%
\contentsline {section}{\numberline {3.6}Statistical Treatment}{5}{section.3.6}%
\contentsline {chapter}{\chapternumberline {4}Results and Discussion}{6}{chapter.4}%
\contentsline {section}{\numberline {4.1}Presentation of Data}{6}{section.4.1}%
\contentsline {section}{\numberline {4.2}Discussion}{6}{section.4.2}%
\contentsline {chapter}{\chapternumberline {5}Summary, Conclusion, and Recommendations}{7}{chapter.5}%
\contentsline {section}{\numberline {5.1}Summary}{7}{section.5.1}%
\contentsline {section}{\numberline {5.2}Conclusion}{7}{section.5.2}%
\contentsline {section}{\numberline {5.3}Recommendations}{7}{section.5.3}%
\contentsline {chapter}{Bibliography}{8}{appendix*.6}%
