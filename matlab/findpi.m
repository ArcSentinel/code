function pos = findpi(num,acc)
%--------------------------------------------------------------------------
%       USAGE: findpi(num,acc)
%
%      AUTHOR: Je Sian Keith Ll. Herman     DATE:   Mar 29, 2020
%    MODIFIED: Je Sian Keith Ll. Herman     DATE:   Mar 29, 2020
%   
% DESCRIPTION: This function finds a sequence of numbers given by the user
%              in the digits of pi (whose number of digits are also given
%              by the user) and outputs the position in pi of the first  
%              digit of the string.
%   
%      INPUTS: num = string: <number sequence to find>
%              acc = double: <The accuracy of pi or number of digits to 
%                             evaluate>
%
%     OUTPUTS: pos = double: <the position of the first digit of the number
%                             sequence in pi>
%
%  REFERENCES: None
%--------------------------------------------------------------------------

digits(acc);                    % Specify the digit accuracy of MATLAB.
accpi = vpa(pi);                % Increase the accuracy and digits of pi.

strpi = char(accpi);            % Change pi to a string.
pos = strfind(strpi, num) - 1;  % Find the position of num and subtract 1
                                % because the period is included in the
                                % counting.
    
fprintf('It is at the %d', pos);  % Display a message announcing the 
fprintf('th digit of pi');           % position of num
end

