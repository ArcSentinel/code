function [surface_area,vol] = cylinderdatafunc(radius,height)
surface_area = 2*pi*radius*(radius+height);  % using the formulas of the
                                             % cylinder, the surface area
                                             % and volume of the cylinder
vol = pi*(radius^2)*height;                  % is calculated
											 % input radius and height.
end