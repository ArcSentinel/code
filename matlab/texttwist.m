clear;
clc;
inputstr = ['ArcSentinel0']      % input a string.
lengthstr = length(inputstr);   % get the number of characters in the
                                % string and store it.
a = flip(inputstr)              % flip the string given.
random = randperm(lengthstr);   % generate a permutation of the set of
                                % numbers from 1 to lengthstr and store it
                                % to a vector called random.
b = inputstr(random)            % use random as the index to call
                                % characters from inputstr and generate
                                % a new vector that is a permutation of 
                                % inputstr and store it to a vector b.
c = lengthstr                   % store lengthstr to a sclar c.