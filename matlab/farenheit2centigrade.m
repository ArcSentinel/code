function tempC = farenheit2centigrade(tempF)
tempC = (5/9)*(tempF-32);
end